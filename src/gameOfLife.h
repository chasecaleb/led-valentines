#ifndef _game_of_life_h
#define _game_of_life_h
#include "config.h"

/* Seed/initialize grid with pseudo-prandom values. */
void seedGrid(uint8_t grid[GRID_SIZE][GRID_SIZE]);

void iterateGrid(uint8_t grid[GRID_SIZE][GRID_SIZE]);

/*
 * Return 1 if cell shoud be alive, 0 if not.
 */
uint8_t cellIsAlive(uint8_t grid[GRID_SIZE][GRID_SIZE], int row, int col);

/*
 * Return 1 if pattern is in a repeated loop, 0 if not.
 */
uint8_t isCycle(uint8_t grid[GRID_SIZE][GRID_SIZE]);

uint8_t countCells(uint8_t grid[GRID_SIZE][GRID_SIZE]);

#endif
