#include <stdint.h>
#include "LPC810_CodeBase/cmsis/LPC8xx.h"
#include "gameOfLife.h"
#include "random.h"

void seedGrid(uint8_t grid[GRID_SIZE][GRID_SIZE]) {
    /* Divisor, so larger values of probability equal less likely. */
    /* uint32_t probability = 3 + countCells(grid)/2; */
    uint32_t probability = 3 + countCells(grid)*countCells(grid)/2;
    if (probability > 10) probability = 10;

    for (int x = 0; x < GRID_SIZE; x++) {
        for (int y = 0; y < GRID_SIZE; y++) {
            /* This is ugly, but it works and avoids double arithmetic. */
            const uint8_t isAlive = (random() >> 16) < (1 << 16) / probability;
            /* If cell is already alive, this won't change that. */
            grid[x][y] |= (isAlive != 0) ? 1 : 0;
        }
    }
}

void iterateGrid(uint8_t grid[GRID_SIZE][GRID_SIZE]) {
    static uint_fast16_t count;
    uint8_t original[GRID_SIZE][GRID_SIZE];
    /* Make a copy of unmodified grid, used to determine which cell lives. */
    for (int x = 0; x < GRID_SIZE; x++) {
        for (int y = 0; y < GRID_SIZE; y++) {
            original[x][y] = grid[x][y];
        }
    }

    for (int x = 0; x < GRID_SIZE; x++) {
        for (int y = 0; y < GRID_SIZE; y++) {
            grid[x][y] = cellIsAlive(original, x, y);
        }
    }

    /* Reseed once in a while in case of cycles. */
    if (count++ > FRAMERATE * 60 ) {
        count = 0;
        seedGrid(grid);
    }
}

/*
 * Rules:
 * Live cell with < 2 or > 3 neighbors dies.
 * Live cell with 2 or 3 neighbors lives.
 * Dead cell with 3 neighbors comes alive.
 */
uint8_t cellIsAlive(uint8_t grid[GRID_SIZE][GRID_SIZE], int row, int col) {
    /* Cell/Neighbors:
     * (x-1, y-1) (x, y-1) (x+1, y-1)
     * (x-1, y)   (x,y)    (x+1, y)
     * (x-1, y+1) (x, y+1) (x+1, y+1)
     */
    uint8_t neighbors = 0;
    for (int x = row-1; x <= row+1; x++) {
        for (int y = col-1; y <= col+1; y++) {
            if (x == row && y == col) continue;  /* cell itself, not neighbor. */

            const int modX = ((x % GRID_SIZE) + GRID_SIZE) % GRID_SIZE;
            const int modY = ((y % GRID_SIZE) + GRID_SIZE) % GRID_SIZE;
            if (grid[modX][modY] != 0) {
                neighbors++;
            }
        }
    }

    if (grid[row][col] != 0) {  /* Cell started out alive */
        return (neighbors == 2 || neighbors == 3);
    } else {  /* Dead cell */
        return neighbors == 3;
    }
}

uint8_t isCycle(uint8_t grid[GRID_SIZE][GRID_SIZE]) {
    /* Recent states stored as bit fields.
     * GRID_SIZE*GRID_SIZE total bits required and each uint32_t holds (wait
     * for it) 32 bits, so base inner array size off that.
     * */
    static uint32_t states[MAX_CYCLE][(GRID_SIZE*GRID_SIZE)/32];
    const uint32_t stateSize = (GRID_SIZE*GRID_SIZE)/32;

    /* Shift array */
    for (int x = 1; x < MAX_CYCLE; x++) {
        for (int y = 0; y < stateSize; y++) {
            states[x-1][y] = states[x][y];
        }
    }

    /* Zero oldest and save current state */
    for (int x = 0; x < stateSize; x++){
        states[MAX_CYCLE-1][x] = 0;
    }
    for (int i = 0; i < GRID_SIZE*GRID_SIZE; i++) {
        if (grid[i/7][i%7] != 0) states[MAX_CYCLE-1][i/32] |= 1 << (i%32);
    }

    /* Determine whether current state matches any previous */
    for (int s = 0; s < MAX_CYCLE - 1; s++) {
        uint8_t isUnique = 0;
        for (int i = 0; i < stateSize; i++) {
            if (states[s][i] != states[MAX_CYCLE-1][i]) {
                isUnique = 1;
                break;
            }
        }
        if (isUnique == 0) return 1;  /* Cycle detected */
    }
    return 0;  /* No matches (cycles) found */
}

uint8_t countCells(uint8_t grid[GRID_SIZE][GRID_SIZE]) {
    uint8_t count = 0;
    for (int x = 0; x < GRID_SIZE; x++) {
        for (int y = 0; y < GRID_SIZE; y++) {
            if (grid[x][y]) count++;
        }
    }
    return count;
}
