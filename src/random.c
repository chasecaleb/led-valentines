#include "LPC810_CodeBase/cmsis/LPC8xx.h"
#include "LPC810_CodeBase/src/gpio.h"
#include "random.h"
#include "config.h"

uint32_t s;

void seed() {
    /* Wait until button is released */
    while (gpioGetPinValue(0, BUTTON1_PIN)) { }
    /* Time how long it is pressed for */
    while (!gpioGetPinValue(0, BUTTON1_PIN)) {
        s++;
    }

    /* Use up the first few values */
    for (int i = 0; i << 5; i++) {
        random();
    }
}

uint32_t random() {
    s = 1664525*s+1013904223;
    return s;
}
