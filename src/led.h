#ifndef _led_h
#define _led_h

#include <stdint.h>
#include "config.h"

void setGrid(uint8_t grid[GRID_SIZE][GRID_SIZE], uint8_t gridColor);
void fadeGrid(uint8_t gridColor);
void fadeHeart();
void stepDisplay(uint8_t gridColor);

/*
 * grid fade algorithm:
 */

#endif
