#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include "gameOfLife.h"

void printGrid(uint8_t grid[GRID_SIZE][GRID_SIZE]);

int main() {
    srand(time(NULL));
    /* srand(1); */

/*
 *     uint8_t grid[GRID_SIZE][GRID_SIZE] = {0};
 *     seedGrid(grid, ALIVE_PROB);
 *     printGrid(grid);
 * 
 *     int iters = 1000;
 *     int reseed = 0;
 *     for (int i = 0; i < iters; i++) {
 *         printGrid(grid);
 *         iterateGrid(grid);
 *         if (isCycle(grid)) {
 *             seedGrid(grid, RESEED_PROB);
 *             reseed++;
 *         }
 *         printf("iters %d, reseeds %d\n", i, reseed);
 *         i++;
 *     }
 *     printf("Average iterations per reseed: %d\n", iters/reseed);
 */

/*
 *     int bestSeed = 0;
 *     int bestCount = 0;
 *     int iters = 100000;
 *     int denom = 100;
 *     for (int s = 1; s < denom; s++) {
 *         printf("%d\n", s);
 *         uint8_t grid[GRID_SIZE][GRID_SIZE] = {0};
 *         seedGrid(grid, (double)s/(double)denom);
 * 
 *         int count = 0;
 *         for (int i = 0; i < iters; i++) {
 *             while (!isCycle(grid)) {
 *                 iterateGrid(grid);
 *                 count++;
 *             }
 *         }
 *         if (count > bestCount) {
 *             bestSeed = s;
 *             bestCount = count;
 *         }
 *     }
 */
    long long sum = 0;
    int iters = 2000;
    for (int n = 0; n < iters; n++) {
        uint8_t grid[GRID_SIZE][GRID_SIZE];
        seedGrid(grid, ALIVE_PROB);
        for (int i = 0; i < iters; i++) {
            iterateGrid(grid);
            for (int x = 0; x < GRID_SIZE; x++) {
                for (int y = 0; y < GRID_SIZE; y++) {
                    sum += grid[x][y];
                }
            }
        }
        if (n % 100 == 0) printf("%d\n", n);
    }
    int avg = sum / (iters*iters);
    printf("Avg is: %d\n", (avg));
}

void printGrid(uint8_t grid[GRID_SIZE][GRID_SIZE]) {
    printf("  0 1 2 3 4 5 6\n");
    for (int x = 0; x < GRID_SIZE; x++) {
        printf("%d ", x);
        for (int y = 0; y < GRID_SIZE; y++) {
            if (grid[x][y] != 0) {
                printf("* ");
            } else {
                printf("  ");
            }
        }
        printf("\n");
    }
    printf("\n\n");
}
