#ifndef _characters_h
#define _characters_h
#include <stdint.h>

// This bit field exists for the sake of memory saving.
struct CharacterColumn {
    uint8_t zero : 1;
    uint8_t one : 1;
    uint8_t two : 1;
    uint8_t three : 1;
    uint8_t four : 1;
    uint8_t five : 1;
    uint8_t six : 1;
};

struct Character {
    struct CharacterColumn pixels[7];
};

struct Character charToStruct(char c);
#endif
