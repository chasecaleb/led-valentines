#include "LPC810_CodeBase/cmsis/LPC8xx.h"
#include "LPC810_CodeBase/src/mrt.h"
#include "light_ws2812_cortex.h"
#include "led.h"
#include "gameOfLife.h"

struct Display {
    uint8_t grid[GRID_SIZE][GRID_SIZE][3];
    struct Led heart[HEART_SIZE];
};

struct Display disp;

void setGrid(uint8_t grid[GRID_SIZE][GRID_SIZE], uint8_t gridColor) {
    static uint8_t lastColor;
    if (gridColor != lastColor) {
        for (int x = 0; x < GRID_SIZE; x++) {
            for (int y = 0; y < GRID_SIZE; y++) {
                disp.grid[x][y][0] = 0;
                disp.grid[x][y][1] = 0;
                disp.grid[x][y][2] = 0;
                grid[x][y] = 0;
            }
        }
        seedGrid(grid);
        lastColor = gridColor;
    }

    /* Rows in correct order */
    for (int x = 0; x < GRID_SIZE; x += 2) {
        for (int y = 0; y < GRID_SIZE; y++) {
            /* Only modify set if cell is dead */
            if (grid[x][y] && disp.grid[x][y][gridColor] == 0) {
                    disp.grid[x][y][gridColor] = INIT_BRIGHTNESS;
            } else {
                disp.grid[x][y][gridColor] = 0;
            }
        }
    }
    /* Reversed rows */
    for (int x = 1; x < GRID_SIZE; x += 2) {
        for (int y = 0; y < GRID_SIZE; y++) {
            if (grid[x][y] && disp.grid[x][y][gridColor] == 0) {
                disp.grid[x][6-y][gridColor] = INIT_BRIGHTNESS;
            } else {
                disp.grid[x][y][gridColor] = 0;
            }
        }
    }
}

void fadeGrid(uint8_t gridColor) {
    for (int x = 0; x < GRID_SIZE; x++) {
        for (int y = 0; y < GRID_SIZE; y++) {
            if (disp.grid[x][y][gridColor] == 0) continue;
            if (disp.grid[x][y][gridColor] == MAX_BRIGHTNESS) continue;

            const uint8_t initial = disp.grid[x][y][gridColor];
            disp.grid[x][y][gridColor] += MAX_BRIGHTNESS/FRAMERATE;
            /* Correct for overflow */
            if (disp.grid[x][y][gridColor] < initial) {
                disp.grid[x][y][gridColor] = MAX_BRIGHTNESS;
            }
        }
    }
}

void fadeHeart() {
    static struct Led color = {0, INIT_BRIGHTNESS, 0};
    static uint_fast8_t doIncrease = 1;
    if (doIncrease) {
        color.red++;
    } else {
        color.red--;
    }
    if (color.red == INIT_BRIGHTNESS>>1 || color.red == MAX_BRIGHTNESS) doIncrease = !doIncrease;

    for (int i = 0; i < HEART_SIZE; i++) {
        /* Copied like this because disp.heart[i] = color calls missing memcpy */
        disp.heart[i].green = color.green;
        disp.heart[i].red = color.red;
        disp.heart[i].blue = color.blue;
    }
}

void stepDisplay(uint8_t gridColor) {
    fadeHeart();
    fadeGrid(gridColor);

    /* Without interrupts disabled, flicker/wrong colors are displayed due to
     * timing sensitivity. */
    __disable_irq();
    /* Cast to uint8_t pointer (array), this will also allow access to
     * disp.heart since they are guaranteed to be next to each other in memory.
     * */
    ws2812_sendarray((uint8_t *)disp.grid, LED_TOTAL);
    __enable_irq();
}
