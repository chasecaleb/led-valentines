#include <stdint.h>
#include "LPC810_CodeBase/cmsis/LPC8xx.h"
#include "LPC810_CodeBase/src/gpio.h"
#include "LPC810_CodeBase/src/mrt.h"
#include "config.h"
#include "gameOfLife.h"
#include "random.h"
#include "led.h"

uint8_t grid[GRID_SIZE][GRID_SIZE];

/*
 * Because apparently memset is missing
 */
void zero(uint8_t *arr, unsigned int len) {
    for (unsigned int i = 0; i < len; i++) {
        arr[i] = 0;
    }
}

void setup() {
    /* Configure the multi-rate timer for 1ms ticks */
    mrtInit(__SYSTEM_CLOCK/1000);
    /* Initialise the GPIO block */
    gpioInit();
    /* Disable SWDCLK and SWDIO to enable more GPIO's */
    LPC_SWM->PINENABLE0 = 0xffffffbfUL;
    /* Set LED pin to output, buttons to inputs */
    gpioSetDir(0, LED_PIN, 1);
    gpioSetDir(0, BUTTON1_PIN, 0);
    gpioSetDir(0, BUTTON2_PIN, 0);
}

int main(void)
{
    setup();

    uint8_t speedModifier = DEFAULT_SPEED;
    uint8_t color = DEFAULT_COLOR;

    zero((uint8_t *)grid, GRID_SIZE*GRID_SIZE);
    setGrid(grid, color);
    stepDisplay(color);

    seed();
    seedGrid(grid);

    /* Because of seed() requiring button press on startup. */
    while (!gpioGetPinValue(0, BUTTON1_PIN)) ;

    while (1) {
        if (!gpioGetPinValue(0, BUTTON1_PIN)) {
            speedModifier++;
            if (speedModifier > 3) speedModifier = 1;
            while (!gpioGetPinValue(0, BUTTON1_PIN)) ;
        }
        if (!gpioGetPinValue(0, BUTTON2_PIN)) {
            if (color == 2) {
                color = 1;
            } else {
                color = 2;
            }
            setGrid(grid, color);
            while (!gpioGetPinValue(0, BUTTON2_PIN)) ;
        }

        if (isCycle(grid)) {
            /* Probability gets lowered depending on how many cells are still
             * alive, since a cycle with many cells alive should only need a
             * small nudge. */
            seedGrid(grid);
        }
        iterateGrid(grid);
        setGrid(grid, color);
        for (int i = 0; i < 5; i++) {
            stepDisplay(color);
            mrtDelay(1000/(speedModifier*FRAMERATE/2));
        }
    }
}
