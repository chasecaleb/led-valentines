#include "characters.h"

struct Character charToStruct(char c) {
    struct Character s;

    // Initialize pixels to 0 here, then set 1's in switch statement.
    for (int i = 0; i < 7; i++) {
        const struct CharacterColumn col = {0};
        s.pixels[i] = col;
    }

    // I can't believe I'm typing all this nonsense out...
    // All praise the mighty Vim.
    switch (c) {
        // Letters.
        case 'A':
        case 'a':
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].four = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].four = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].four = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].three = 1;
            s.pixels[5].four = 1;
            s.pixels[5].five = 1;
            s.pixels[5].six = 1;
            break;
        case 'B':
        case 'b':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].three = 1;
            s.pixels[2].six = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].three = 1;
            s.pixels[3].six = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].three = 1;
            s.pixels[4].six = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].four = 1;
            s.pixels[5].five = 1;
            break;
        case 'C':
        case 'c':
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].six = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].six = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].six = 1;
            s.pixels[5].one = 1;
            s.pixels[5].five = 1;
            break;
        case 'D':
        case 'd':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].six = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].six = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].six = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].three = 1;
            s.pixels[5].four = 1;
            s.pixels[5].five = 1;
            break;
        case 'E':
        case 'e':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].three = 1;
            s.pixels[2].six = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].three = 1;
            s.pixels[3].six = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].three = 1;
            s.pixels[4].six = 1;
            s.pixels[5].zero = 1;
            s.pixels[5].six = 1;
            break;
        case 'F':
        case 'f':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].three = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].three = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].three = 1;
            s.pixels[5].zero = 1;
            break;
        case 'G':
        case 'g':
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].six = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].six = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].four = 1;
            s.pixels[4].six = 1;
            s.pixels[5].one = 1;
            s.pixels[5].four = 1;
            s.pixels[5].five = 1;
            break;
        case 'H':
        case 'h':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].two = 1;
            s.pixels[3].two = 1;
            s.pixels[4].two = 1;
            s.pixels[5].zero = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].three = 1;
            s.pixels[5].four = 1;
            s.pixels[5].five = 1;
            s.pixels[5].six = 1;
            break;
        case 'I':
        case 'i':
            s.pixels[1].zero = 1;
            s.pixels[1].six = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].six = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].one = 1;
            s.pixels[3].two = 1;
            s.pixels[3].three = 1;
            s.pixels[3].four = 1;
            s.pixels[3].five = 1;
            s.pixels[3].six = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].six = 1;
            s.pixels[5].zero = 1;
            s.pixels[5].six = 1;
            break;
        case 'J':
        case 'j':
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[2].six = 1;
            s.pixels[3].six = 1;
            s.pixels[4].six = 1;
            s.pixels[5].zero = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].three = 1;
            s.pixels[5].four = 1;
            s.pixels[5].five = 1;
            break;
        case 'K':
        case 'k':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].three = 1;
            s.pixels[3].two = 1;
            s.pixels[3].four = 1;
            s.pixels[4].one = 1;
            s.pixels[4].five = 1;
            s.pixels[5].zero = 1;
            s.pixels[5].six = 1;
            break;
        case 'L':
        case 'l':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].six = 1;
            s.pixels[3].six = 1;
            s.pixels[4].six = 1;
            s.pixels[5].six = 1;
            break;
        case 'M':
        case 'm':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].one = 1;
            s.pixels[3].two = 1;
            s.pixels[4].one = 1;
            s.pixels[5].zero = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].three = 1;
            s.pixels[5].four = 1;
            s.pixels[5].five = 1;
            s.pixels[5].six = 1;
            break;
        case 'N':
        case 'n':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].two = 1;
            s.pixels[3].three = 1;
            s.pixels[4].four = 1;
            s.pixels[5].zero = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].three = 1;
            s.pixels[5].four = 1;
            s.pixels[5].five = 1;
            s.pixels[5].six = 1;
            break;
        case 'O':
        case 'o':
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].six = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].six = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].six = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].three = 1;
            s.pixels[5].four = 1;
            s.pixels[5].five = 1;
            break;
        case 'P':
        case 'p':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].four = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].four = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].four = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].three = 1;
            break;
        case 'Q':
        case 'q':
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].six = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].four = 1;
            s.pixels[3].six = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].five = 1;
            s.pixels[4].six = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].three = 1;
            s.pixels[5].four = 1;
            s.pixels[5].five = 1;
            s.pixels[5].six = 1;
            break;
        case 'R':
        case 'r':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].four = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].four = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].four = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].three = 1;
            s.pixels[5].five = 1;
            s.pixels[5].six = 1;
            break;
        case 'S':
        case 's':
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].five = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].three = 1;
            s.pixels[2].six = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].three = 1;
            s.pixels[3].six = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].three = 1;
            s.pixels[4].six = 1;
            s.pixels[5].one = 1;
            s.pixels[5].four = 1;
            s.pixels[5].five = 1;
            break;
        case 'T':
        case 't':
            s.pixels[1].zero = 1;
            s.pixels[2].zero = 1;
            s.pixels[3].zero = 1;
            s.pixels[4].zero = 1;
            s.pixels[5].zero = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].one = 1;
            s.pixels[3].two = 1;
            s.pixels[3].three = 1;
            s.pixels[3].four = 1;
            s.pixels[3].five = 1;
            s.pixels[3].six = 1;
            break;
        case 'U':
        case 'u':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[2].six = 1;
            s.pixels[3].six = 1;
            s.pixels[4].six = 1;
            s.pixels[5].zero = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].three = 1;
            s.pixels[5].four = 1;
            s.pixels[5].five = 1;
            break;
        case 'V':
        case 'v':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[2].five = 1;
            s.pixels[3].six = 1;
            s.pixels[4].five = 1;
            s.pixels[5].zero = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].three = 1;
            s.pixels[5].four = 1;
            break;
        case 'W':
        case 'w':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[1].three = 1;
            s.pixels[1].four = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].five = 1;
            s.pixels[3].four = 1;
            s.pixels[4].five = 1;
            s.pixels[5].zero = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            s.pixels[5].three = 1;
            s.pixels[5].four = 1;
            s.pixels[5].five = 1;
            s.pixels[5].six = 1;
            break;
        case 'X':
        case 'x':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].two = 1;
            s.pixels[2].four = 1;
            s.pixels[3].three = 1;
            s.pixels[4].two = 1;
            s.pixels[4].four = 1;
            s.pixels[5].zero = 1;
            s.pixels[5].one = 1;
            s.pixels[5].five = 1;
            s.pixels[5].six = 1;
            break;
        case 'Y':
        case 'y':
            s.pixels[1].zero = 1;
            s.pixels[1].one = 1;
            s.pixels[1].two = 1;
            s.pixels[2].three = 1;
            s.pixels[3].four = 1;
            s.pixels[3].five = 1;
            s.pixels[3].six = 1;
            s.pixels[4].three = 1;
            s.pixels[5].zero = 1;
            s.pixels[5].one = 1;
            s.pixels[5].two = 1;
            break;
        case 'Z':
        case 'z':
            s.pixels[1].zero = 1;
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].zero = 1;
            s.pixels[2].four = 1;
            s.pixels[2].six = 1;
            s.pixels[3].zero = 1;
            s.pixels[3].three = 1;
            s.pixels[3].six = 1;
            s.pixels[4].zero = 1;
            s.pixels[4].two = 1;
            s.pixels[4].six = 1;
            s.pixels[5].zero = 1;
            s.pixels[5].one = 1;
            s.pixels[5].six = 1;
            break;

        // Symbols/misc.
        case ' ':
            // Yay.
            break;
        case ',':
            s.pixels[0].six = 1;
            s.pixels[1].five = 1;
            s.pixels[2].four = 1;
        case '.':
            s.pixels[1].five = 1;
            s.pixels[1].six = 1;
            s.pixels[2].five = 1;
            s.pixels[2].six = 1;
            break;

        default:
            break;
    }

    return s;
}
