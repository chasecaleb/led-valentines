#include <stdio.h>
#include "characters.h"

// Here lies test code nearly as ugly as the real code.

char convert(uint8_t i) {
    return (i == 1) ? '*' : ' ';
}

void print(char passed) {
    struct Character c = charToStruct(passed);
    for (int i = 0; i < 7; i++) {
        printf("%c", convert(c.pixels[i].zero));
    }
    printf("\n");
    for (int i = 0; i < 7; i++) {
        printf("%c", convert(c.pixels[i].one));
    }
    printf("\n");
    for (int i = 0; i < 7; i++) {
        printf("%c", convert(c.pixels[i].two));
    }
    printf("\n");
    for (int i = 0; i < 7; i++) {
        printf("%c", convert(c.pixels[i].three));
    }
    printf("\n");
    for (int i = 0; i < 7; i++) {
        printf("%c", convert(c.pixels[i].four));
    }
    printf("\n");
    for (int i = 0; i < 7; i++) {
        printf("%c", convert(c.pixels[i].five));
    }
    printf("\n");
    for (int i = 0; i < 7; i++) {
        printf("%c", convert(c.pixels[i].six));
    }
    printf("\n");
    printf("\n");
    printf("\n");
}

int main() {
    print('a');
    print('A');
    print('b');
    print('B');
    print('c');
    print('C');
    print('d');
    print('D');
    print('e');
    print('E');
    print('f');
    print('F');
    print('g');
    print('G');
    print('h');
    print('H');
    print('i');
    print('I');
    print('j');
    print('J');
    print('k');
    print('K');
    print('l');
    print('L');
    print('m');
    print('M');
    print('n');
    print('N');
    print('o');
    print('O');
    print('p');
    print('P');
    print('q');
    print('Q');
    print('r');
    print('R');
    print('s');
    print('S');
    print('t');
    print('T');
    print('u');
    print('U');
    print('v');
    print('V');
    print('w');
    print('W');
    print('x');
    print('X');
    print('y');
    print('Y');
    print('z');
    print('Z');
}
