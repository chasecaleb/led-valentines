#ifndef _config_h
#define _config_h

#include <stdint.h>

#define GRID_SIZE           7
#define HEART_SIZE          10
#define LED_TOTAL           (3 * (GRID_SIZE*GRID_SIZE + HEART_SIZE))

#define MAX_BRIGHTNESS      250
#define INIT_BRIGHTNESS     (MAX_BRIGHTNESS>>2)  /* 25% of max */
#define FRAMERATE           30
#define DEFAULT_SPEED       2

#define LED_PIN             3  /* PIO0_3/physical pin 3 */
#define BUTTON1_PIN         1  /* PIO0_2/physical pin 4 */
#define BUTTON2_PIN         2  /* PIO0_1/physical pin 5 */

/* Game of Life config */
#define MAX_CYCLE           3   /* Number of iterations to check for repeated cycles. */
#define DEFAULT_COLOR       2

struct Led{
    uint8_t green;
    uint8_t red;
    uint8_t blue;
};

#endif
