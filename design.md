# General function
Alternate between message, heart animation, and game of life simulation.

# Design
* Heart animation
    * This should (hopefully) be done with the leftover LED's, not on the matrix
      itself.
    * Simple and elegant -- slowly "beating" (fading in/out from middle)
* Game of life simulation
* Message display: "For my penguin"
    * Design pixel font... by which I mean find one online.
    * Store alphabet/symbol list in flash as a struct with: int width,
      bool[7][7] representation.
    * Implement display(string msg, int color) func.
        * Calculate total width by adding width of each character together +
          buffer in between each.
        * Convert passed msg from string to matrix with dimensions 7 x total_width.
        * Scroll: light up next 7x7 leds from current position, delay, increment
          position.

# Misc/Notes
* **Important:**  For some reason, when both LPC810 and the LED's are running at
  3.5v, they flicker the wrong color a fair amount. Dropping the voltage way
  down to 2.2V more or less fixes that. Considering the LEDs are spec'ed at
  4.5v+... this makes no sense.

# TODO
* Make matrix fade between more than just red.
* Add color to heart animation.
* 2 buttons from: brightness, speed, color scheme
* Use fast/least data types
* Store prng seed() value to flash.
* display improvement: fade dead cells instead of instant off.
* scrolling message ... so much time though.
